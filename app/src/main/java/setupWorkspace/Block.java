package setupWorkspace;

import com.google.common.hash.Hashing;

import java.nio.charset.StandardCharsets;

public class Block {

    private final String hash;
    private final String payload;
    private final String previous;

    public Block(String payload, String previous) {

        this.hash = previous == null ? buildHash(payload ) : buildHash(payload + previous);
        this.payload = payload;
        this.previous =  previous;

    }

    private String buildHash(String input) {
        return Hashing.sha256()
                .hashString(input, StandardCharsets.UTF_8)
                .toString();
    }


    public String getHash() {
        return this.hash;
    }

    public String getPayload() {
        return this.payload;
    }

    public String getPrevious() {
        return previous;
    }


}
