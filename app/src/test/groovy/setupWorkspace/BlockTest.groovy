package setupWorkspace

import org.assertj.core.api.AssertJProxySetup
import org.junit.Assert
import spock.lang.Specification

class BlockTest extends Specification {
    def payload1 = "Hello blockchain!"
    def hash1 = "1dca8bd16bf99b132166245114ffab14af8fa4b0e2d964821d37800f229a3460"

    def buildBlock0(){
        return new Block(payload1,null)
    }

    def "A block exist"() {
        setup:
        def block
        when:
        block = buildBlock0()
        then:
        block != null
    }

    def "A block has a id, hash and payload"() {
        setup:
        def block

        when:
        block = buildBlock0()
        then:

        block.getHash().equals(hash1)
        block.getPayload().equals(payload1)
    }

    def "A block has a link to a previous block"() {
        setup:
        def block1 = buildBlock0()

        when:
        def block2 = new Block("payload", block1.getHash())
        then:

        block2.getPrevious().equals(block1.getHash())
    }

    def "A block #0 has a no previous block"() {
        setup:

        given:

        when:
            def block0 = buildBlock0()
        then:
            Assert.assertNull(block0.getPrevious())
    }

    def "Block 0# The hash is the based on the payload"() {
        when:
        def block0 = buildBlock0()
        then:

        Assert.assertEquals(block0.getHash(),hash1)
    }

    def "For NON Block 0# The hash is the based on the payload + previus hash"() {
        given:
        def block0 = buildBlock0()
        def block1 = new Block("Second block",block0.getHash())
        when:
        def newHash = block1.getHash()
        then:

        Assert.assertEquals(newHash,"34209d9b6672d379159b3bdfe73350c4922d0efa19aa6a5439f7fb6557024201")
    }









}
